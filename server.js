const app = require('./app')
const mongoose = require('mongoose')

require('dotenv').config({path:'variables.env'})


// conect database 
// teste de envio pelo editor
mongoose.connect(process.env.DATABASE, {useNewUrlParser: true})
mongoose.Promise = global.Promise
mongoose.connection.on('error', (error) => {
    console.log(`erro ${error.message}`)
})

app.set('port', process.env.PORT || 7777)
const server = app.listen(app.get('port'), () => {
    console.log(`server rodando na porta ${server.address().port}`)
})